package data

import (
	model "gitlab/rohitpjpti18/gowithgraph/models"
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var Db *gorm.DB

func Init() {
	log.Println("DataBase Connection Initiated")
	dsn := os.Getenv("gographrdbms")
	var err error

	Db, err = gorm.Open(postgres.Open(dsn), &gorm.Config{
		NamingStrategy: schema.NamingStrategy{
			TablePrefix: "webapi.",
		},
	})

	if err != nil {
		log.Fatalln("DataBase Connection Failed: ", err)
	}

	err = Migrate()
	if err != nil {
		log.Fatalln("DataBase Migration Failed: ", err)
	}

	log.Println("DataBase Connection Successfull")
}

func Migrate() error {
	return Db.AutoMigrate(&model.Address{},
		&model.Author{},
		&model.Publisher{},
		&model.Author{},
		&model.BookInfo{},
		&model.Book{})
}
