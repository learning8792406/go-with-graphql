package models

import (
	"time"
)

type Book struct {
	ID          uint64    `json:"id,omitempty" gorm:"primaryKey,autoIncrement"`
	BookCode    uint32    `json:"book_code,omitempty"`
	IsBorrowed  bool      `json:"is_borrowed"`
	AvailableBy time.Time `json:"available_by,omitempty"`
	BookId      uint64    `json:"book_id,omitempty"`
	Book        BookInfo  `json:"book_details,omitempty" gorm:"foreignKey:BookId;references:ID"`
	LibraryId   uint64    `json:"library_id,omitempty"`
	Library     Library   `json:"library_details,omitempty" gorm:"foreignKey:LibraryId;references:ID"`
}
