package models

const (
	HOME      string = "HOME"
	CORPORATE string = "CORPORATE"
	OFFICE    string = "OFFICE"
	LEGAL     string = "LEGAL"
	PERSONAL  string = "PERSONAL"
)

type Address struct {
	ID          uint64 `json:"id,omitempty" gorm:"primaryKey,autoIncrement"`
	AddressType string `json:"address_type,omitempty"`
	Line1       string `json:"address_line,omitempty"`
	Town        string `json:"city_town_village,omitempty"`
	District    string `json:"district,omitempty"`
	State       string `json:"state,omitempty"`
	Country     string `json:"country,omitempty"`
}
