package models

import (
	"time"
)

type Library struct {
	ID                 uint64           `json:"id,omitempty" gorm:"primaryKey,autoIncrement"`
	Name               string           `json:"book_code,omitempty"`
	ContactPersonId    uint64           `json:"contact_person_id"`
	CreatedOn          time.Time        `json:"available_by,omitempty"`
	IsActive           bool             `json:"is_active"`
	AddressId          uint64           `json:"address_id,omitempty"`
	Address            Address          `json:"address,omitempty" gorm:"foreignKey:AddressId;references:ID"`
	SubscriptionPlanId uint64           `json:"subscription_plan_id,omitempty"`
	SubscriptionPlan   SubscriptionPlan `json:"subscription,omitempty" gorm:"foreignKey:SubscriptionPlanId;references:ID"`
}
