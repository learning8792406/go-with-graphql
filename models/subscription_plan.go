package models

type SubscriptionPlan struct {
	ID   uint64 `json:"id,omitempty" gorm:"primaryKey,autoIncrement"`
	Name string `json:"name,omitempty"`
}
