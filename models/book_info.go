package models

type BookInfo struct {
	ID          uint64    `json:"id,omitempty" gorm:"primaryKey,autoIncrement"`
	Title       string    `json:"title,omitempty"`
	Price       float64   `json:"price,omitempty"`
	AuthorId    uint64    `json:"author_id,omitempty"`
	Author      Author    `json:"author,omitempty" gorm:"foreignKey:AuthorId;references:ID"`
	PublisherId uint64    `json:"publisher_id,omitempty"`
	Publisher   Publisher `json:"publisher,omitempty" gorm:"foreignKey:PublisherId;references:ID"`
}
