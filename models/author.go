package models

type Author struct {
	ID        uint64  `json:"id,omitempty" gorm:"primaryKey,autoIncrement"`
	Name      string  `json:"name,omitempty"`
	AddressId uint64  `json:"address_id,omitempty"`
	Address   Address `json:"address" gorm:"foreignKey:AddressId;references:ID"`
}
