package controllers

import (
	model "gitlab/rohitpjpti18/gowithgraph/models"
	"gitlab/rohitpjpti18/gowithgraph/services"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
)

func GetBookInfo(c *gin.Context) {
	params := c.Request.URL.Query()

	var result []model.BookInfo

	if params["id"] != nil {
		var ids []string = params["id"]

		for i := 0; i < len(ids); i++ {
			values := strings.Split(ids[i], ",")

			for j := 0; j < len(values); j++ {
				val, err := strconv.ParseUint(values[j], 10, 64)
				if err != nil {
					log.Println(err)
					c.IndentedJSON(http.StatusInternalServerError, err)
					return
				}
				bookInfo := model.BookInfo{ID: val}
				services.GetBookInfo(&bookInfo)
				result = append(result, bookInfo)
			}
		}
	}

	c.IndentedJSON(http.StatusOK, result)
}

func GetAuthor(c *gin.Context) {
	params := c.Request.URL.Query()

	var result []model.Author

	if params["id"] != nil {
		var ids []string = params["id"]

		for i := 0; i < len(ids); i++ {
			values := strings.Split(ids[i], ",")

			for j := 0; j < len(values); j++ {
				val, err := strconv.ParseUint(values[j], 10, 64)
				if err != nil {
					log.Println(err)
					c.IndentedJSON(http.StatusInternalServerError, err)
					return
				}
				author := model.Author{ID: val}
				services.GetAuthor(&author)
				result = append(result, author)
			}
		}
	}

	c.IndentedJSON(http.StatusOK, result)
}

func GetPublisher(c *gin.Context) {
	params := c.Request.URL.Query()

	var result []model.Publisher

	if params["id"] != nil {
		var ids []string = params["id"]

		for i := 0; i < len(ids); i++ {
			values := strings.Split(ids[i], ",")

			for j := 0; j < len(values); j++ {
				val, err := strconv.ParseUint(values[j], 10, 64)
				if err != nil {
					log.Println(err)
					c.IndentedJSON(http.StatusInternalServerError, err)
					return
				}
				publisher := model.Publisher{ID: val}
				services.GetPublisher(&publisher)
				result = append(result, publisher)
			}
		}
	}

	c.IndentedJSON(http.StatusOK, result)
}

func GetBook(c *gin.Context) {
	params := c.Request.URL.Query()

	var result []model.Book

	if params["id"] != nil {
		var ids []string = params["id"]

		for i := 0; i < len(ids); i++ {
			values := strings.Split(ids[i], ",")

			for j := 0; j < len(values); j++ {
				val, err := strconv.ParseUint(values[j], 10, 64)
				if err != nil {
					log.Println(err)
					c.IndentedJSON(http.StatusInternalServerError, err)
					return
				}
				book := model.Book{ID: val}
				services.GetBook(&book)
				result = append(result, book)
			}
		}
	}

	c.IndentedJSON(http.StatusOK, result)
}

func AddAddress(c *gin.Context) {
	newAddress := model.Address{}
	c.BindJSON(&newAddress)

	log.Printf("%+v \n", newAddress)
	err := services.AddNewAddress(&newAddress)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, err)
		return
	}

	c.IndentedJSON(http.StatusOK, newAddress)
}

func AddAuthor(c *gin.Context) {
	newAuthor := model.Author{}
	c.BindJSON(&newAuthor)

	log.Printf("%v \n", newAuthor)
	err := services.AddNewAuthor(&newAuthor)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, err)
		return
	}

	c.IndentedJSON(http.StatusOK, newAuthor)
}

func AddPublisher(c *gin.Context) {
	newPublisher := model.Publisher{}
	c.BindJSON(&newPublisher)

	log.Printf("%+v \n", newPublisher)
	err := services.AddNewPublisher(&newPublisher)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, err)
		return
	}

	c.IndentedJSON(http.StatusOK, newPublisher)
}

func AddBookInfo(c *gin.Context) {
	newBookInfo := model.BookInfo{}
	c.BindJSON(&newBookInfo)

	log.Printf("%+v \n", newBookInfo)
	err := services.AddNewBookInfo(&newBookInfo)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, err)
		return
	}

	c.IndentedJSON(http.StatusOK, newBookInfo)
}

func AddBook(c *gin.Context) {
	newBook := model.Book{}
	c.BindJSON(&newBook)

	log.Printf("%+v \n", newBook)
	err := services.AddNewBook(&newBook)
	if err != nil {
		log.Println(err)
		c.IndentedJSON(http.StatusInternalServerError, err)
		return
	}

	c.IndentedJSON(http.StatusOK, newBook)
}
