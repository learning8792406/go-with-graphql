package services

import (
	"gitlab/rohitpjpti18/gowithgraph/data"
	"gitlab/rohitpjpti18/gowithgraph/models"
	"log"

	"gorm.io/gorm/clause"
)

func GetAddress(address *models.Address) error {
	data.Db.Where("id = ?", address.ID).First(&address)

	return nil
}

func GetAuthor(author *models.Author) error {
	data.Db.Where("id = ?", author.ID).Preload("Address").First(&author)

	return nil
}

func GetPublisher(publisher *models.Publisher) error {
	data.Db.Where("id = ?", publisher.ID).Preload("Address").First(&publisher)

	return nil
}

func GetBookInfo(bookInfo *models.BookInfo) error {
	data.Db.Where("id = ?", bookInfo.ID).Preload(clause.Associations).Preload("Author.Address").Preload("Publisher.Address").First(&bookInfo)

	return nil
}

func GetBook(book *models.Book) error {
	data.Db.Where("id = ?", book.ID).Preload(clause.Associations).First(&book)

	return nil
}

func AddNewAddress(newAddress *models.Address) error {
	if newAddress.ID != 0 {
		err := GetAddress(newAddress)
		if err != nil {
			return err
		}
	} else {
		data.Db.Create(&newAddress)
	}

	return nil
}

func AddNewAuthor(newAuthor *models.Author) error {
	if newAuthor.ID != 0 {
		err := GetAuthor(newAuthor)
		if err != nil {
			return err
		}
	} else {
		AddNewAddress(&newAuthor.Address)
		data.Db.Create(newAuthor)
	}

	return nil
}

func AddNewPublisher(newPublisher *models.Publisher) error {
	if newPublisher.ID != 0 {
		err := GetPublisher(newPublisher)
		if err != nil {
			return err
		}
	} else {
		AddNewAddress(&newPublisher.Address)
		data.Db.Create(newPublisher)
	}

	return nil
}

func AddNewBookInfo(newBookInfo *models.BookInfo) error {
	if newBookInfo.ID != 0 {
		err := GetBookInfo(newBookInfo)
		if err != nil {
			return err
		}
	} else {
		AddNewAuthor(&newBookInfo.Author)
		AddNewPublisher(&newBookInfo.Publisher)
		data.Db.Create(newBookInfo)
	}

	log.Println("%v ", newBookInfo)

	return nil
}

func AddNewBook(newBook *models.Book) error {
	if newBook.ID != 0 {
		err := GetBook(newBook)
		if err != nil {
			return err
		}
	} else {
		AddNewBookInfo(&newBook.Book)
		data.Db.Create(newBook)
	}

	return nil
}
