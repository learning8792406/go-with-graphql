package main

import (
	"gitlab/rohitpjpti18/gowithgraph/controllers"
	"gitlab/rohitpjpti18/gowithgraph/data"

	"github.com/gin-gonic/gin"
)

func main() {
	data.Init()

	gin.SetMode(gin.ReleaseMode)

	router := gin.Default()

	router.GET("/book-info", controllers.GetBookInfo)
	router.GET("/author", controllers.GetAuthor)
	router.GET("/publisher", controllers.GetPublisher)
	router.GET("/book", controllers.GetBook)

	router.POST("/address", controllers.AddAddress)
	router.POST("/author", controllers.AddAuthor)
	router.POST("/publisher", controllers.AddPublisher)
	router.POST("/book-info", controllers.AddBookInfo)
	router.POST("/book", controllers.AddBook)

	router.Run("localhost:8080")
}
